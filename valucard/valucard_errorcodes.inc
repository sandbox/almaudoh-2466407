<?php
  return array(
    '000' => 'Approved, balances available',
    '001' => 'Approved, no balances available',
    '003' => 'Approved, additional identification requested',
    '007' => 'Approved administrative transaction',
    '-1'  => 'System Error',
    '00'  => 'Authentication failed ',
    '02'  => 'Attempt Processing',
    '05'  => 'Not enrolled or Element is Missing',
    '050' => 'General',
    '051' => 'Expired card',
    '052' => 'Number of PIN tries exceeded',
    '053' => 'No sharing allowed',
    '055' => 'Invalid transaction',
    '056' => 'Transaction not supported by institution',
    '057' => 'Lost or stolen card',
    '058' => 'Invalid card status',
    '059' => 'Restricted Status',
    '060' => 'Account not found',
    '061' => 'Wrong customer information for payment',
    '062' => 'Customer information format error',
    '063' => 'Prepaid Code not found',
    '064' => 'Bad track information',
    '069' => 'Bad message edit',
    '074' => 'Unable to authorize',
    '075' => 'Invalid credit PAN',
    '076' => 'Insufficient funds',
    '078' => 'Duplicate transaction received',
    '082' => 'Maximum number of times used',
    '085' => 'Balance not allowed',
    '095' => 'Amount over maximum',
    '100' => 'Unable to process',
    '101' => 'Unable to authorize - call issuer',
    '105' => 'Card not supported',
    '200' => 'Invalid account',
    '201' => 'Incorrect PIN',
    '205' => 'Invalid advance amount',
    '209' => 'Invalid transaction code',
    '210' => 'Bad CAVV',
    '211' => 'Bad CVV2',
    '212' => 'Original transaction not found for slip',
    '213' => 'Slip already received',
    '800' => 'Format error',
    '801' => 'Original transaction not found',
    '809' => 'Invalid close transaction',
    '810' => 'Transaction timeout',
    '811' => 'system error',
    '820' => 'Invalid terminal identifier',
    '880' => 'Download has been received in its entirety',
    '881' => 'Download received successfully and there is more data for this download',
    '882' => 'Download aborted(call for service)',
    '897' => 'Invalid cryptogram',
    '898' => 'Invalid MAC',
    '899' => 'Sequence error-resync',
    '900' => 'Pin Tries Limit Exceeded',
    '909' => 'External Decline Special Condition',
    '959' => 'Administrative transaction not supported',
  );
