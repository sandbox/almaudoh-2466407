<?php
// $Id$

/**
 * @file
 * Provides the views integration for the sms ui contact list
 */

/**
 * Describe table structure to Views.
 *
 * This hook should be placed in MODULENAME.views.inc and it will be auto-loaded.
 * This must either be in the same directory as the .module file or in a subdirectory
 * named 'includes'.
 *
 * The full documentation for this hook is in the advanced help.
 * @link http://views-help.doc.logrus.com/help/views/api-tables @endlink
 */
function webpayment_views_data() {
  // Create views integration for sms_ui_contact
  $data['webpayment_transaction']['table'] = array(
  		'group' => t('Webpayment'),
  		  'base' => array(
			  'field' => 'txid',
			  'title' => t('Webpayment transaction'),
			  'help' => t("Log of transactions made through webpayment gateways."),
  		),
  		'join' => array(
			  'user' => array(
			    'left_field' => 'uid',
			    'field' => 'uid',
			  ),
  		),
  );

  // Transaction ID field.
  $data['webpayment_transaction']['txid'] = array(
	  'title' => t('ID'),
	  'help' => t('The transaction ID of the transaction.'),
	  'field' => array(
	    'handler' => 'views_handler_field_numeric',
	    'click sortable' => TRUE,
	  ),
	  'filter' => array(
	    'handler' => 'views_handler_filter_numeric',
	  ),
	  'sort' => array(
	    'handler' => 'views_handler_sort',
	  ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // User ID field.
  $data['webpayment_transaction']['uid'] = array(
	  'title' => t('UserID'),
	  'help' => t('The user ID of the owner of the transaction.'),
	  'field' => array(
	    'handler' => 'views_handler_field_numeric',
	    'click sortable' => TRUE,
	  ),
	  'filter' => array(
	    'handler' => 'views_handler_filter_numeric',
	  ),
	  'sort' => array(
	    'handler' => 'views_handler_sort',
	  ),
	  'argument' => array(
	    'handler' => 'views_handler_argument_numeric',
	  ),
	  'relationship' => array(
	    'base' => 'users',
	    'field' => 'uid',
	    'handler' => 'views_handler_relationship',
	    'label' => t('Transaction Owner'),
	  ),
  );

  // Status of the transaction
  $data['webpayment_transaction']['txkey'] = array(
    'title' => t('Key'),
    'help' => t('The transaction key. Used to identify the source of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Timestamp field.
  $data['webpayment_transaction']['timestamp'] = array(
	  'title' => t('Time'),
	  'help' => t('The timestamp when the transaction was logged.'),
	  'field' => array(
	    'handler' => 'views_handler_field_date',
	    'click sortable' => TRUE,
	  ),
	  'sort' => array(
	    'handler' => 'views_handler_sort_date',
	  ),
	  'filter' => array(
	    'handler' => 'views_handler_filter_date',
	  ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );

  // Transaction amount requested.
  $data['webpayment_transaction']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('The amount requested for in the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Transaction amount approved.
  $data['webpayment_transaction']['txamount'] = array(
    'title' => t('TxAmount'),
    'help' => t('The amount approved for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Transaction charges made.
  $data['webpayment_transaction']['txcharge'] = array(
    'title' => t('TxCharge'),
    'help' => t('The charges applied for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Description of the transaction
  $data['webpayment_transaction']['description'] = array(
    'title' => t('Description'),
    'help' => t('The description of the source of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Status of the transaction
  $data['webpayment_transaction']['status'] = array(
    'title' => t('Status'),
    'help' => t('The status of the transaction. This is a serialized object which can be unserialized and a member displayed.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Gateway through which the transaction was made
  $data['webpayment_transaction']['gateway'] = array(
	  'title' => t('Gateway'),
	  'help' => t('The payment gateway through which the transaction was made.'),
	  'field' => array(
	    'handler' => 'views_handler_field',
	    'click sortable' => TRUE,
	   ),
	  'filter' => array(
	    'handler' => 'views_handler_filter_string',
	  ),
	  'sort' => array(
	    'handler' => 'views_handler_sort',
	  ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Whether the transaction is still open
  $data['webpayment_transaction']['open'] = array(
	  'title' => t('Open'),
	  'help' => t('Whether this transaction is still open (ie. can be used to credit something).'),
	  'field' => array(
	    'handler' => 'views_handler_field_boolean',
	    'click sortable' => TRUE,
	  ),
	  'filter' => array(
	    'handler' => 'views_handler_filter_boolean_operator',
	    'label' => t('Bulklist'),
	    'type' => 'yes-no',
	  ),
	  'sort' => array(
	    'handler' => 'views_handler_sort',
	  ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Action to be carried out on this transaction  
  $data['webpayment_transaction']['action'] = array(
    'title' => t('Action'),
    'help' => t('A relevant action for the current transaction. HTML markup allowed.'),
    'field' => array(
      'handler' => 'views_handler_field_markup',
    ),
  );

  return $data;
}

