    <style>
      #webpayment_message div { margin:10px 0; padding:4px; font-size:1.0em; }
      #webpayment_message > div { background-color: #fcfcec; }
      #webpayment_message div.webpay-message { background-color: #cfc6a3; }
      #webpayment_message div.webpay-error {  }
      #webpayment_message div.webpay-instruction {  }
      #webpayment_message div.wp-success { background-color: #dfeef7; }
      #webpayment_message div.wp-success div.webpay-message { background-color: #699bb6; }
      #webpayment_message div.wp-success div.webpay-error {  }
      #webpayment_message div.wp-success div.webpay-instruction {  }
    </style>
    <div class="<?php print $status_class ?>" style="border:solid 1px #333;">
     <div class="webpay-message" style="font-style:italic; font-weight:bold; margin-top: 0">
       <?php print $message ?>
     </div>
     <div class="webpay-error" style="font-weight:bold;font-size:0.9em;color:#A22">
       <?php print $error ?>
     </div>
     <div class="webpay-instruction" style="color:#333;">
       <?php print $instruction ?>
     </div>
    </div>
