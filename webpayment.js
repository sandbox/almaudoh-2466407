// Js to load a webpayment window into a popup dialog
// !!! Requires popups.js

$(document).ready(function() {
  // Set up the paynow button
  $('#edit-paybutton').val('Pay Now!').bind('click', function() {doPayment(); return false;});

  // Fix for [].indexOf() not available in IE preventing proper popups closure in IE.
  if (typeof(Popups.popupStack.indexOf) != 'function') {
    Popups.popupStack.indexOf = function(popup) {
      for (p in this) {
        if (this[p] == popup) return p;
      }
      return undefined;
    }
  }
  
  // Ajax loading image
  $('#loading-wait').ajaxStart(function() {
    $(this).show();
  });

  $('#loading-wait').ajaxStop(function() {
    $(this).hide('');
  });

  $('#webpayment_message').ajaxSend(function() {
    $(this).text('');
  });
});

/**
 * Namespace for webpay
 */
Webpayment = {
  lock : false,
  popup : null,
  config : null,
  
  closeAction : function(callback) {
    Webpayment.popupdone(callback);
    return Popups.close(Webpayment.popup);
  }  
};

/**
 * Default url for form posted payment gateways
 */
Webpayment.postUrl = function(txid, amount) {
  var url = Drupal.settings.webpayment.postPath + '/' + txid + '/' + amount;
  return url;
};  

/*
 * Initial settings for web payments
 */
Webpayment.configure = function(config) {
  // Save config
  Webpayment.config = config;
}

/*
 * Get configuration settings for a specified payment gateway
 */
Webpayment.getConfig = function(callback) {
  var ajaxOptions = {
    type : "POST",
    url : Drupal.settings.webpayment.configPath,
    data : '',
    success : function(resp, status) {
      if (typeof(callback) == 'function') {
        callback.call(callback, resp);
      }
      else if (typeof(callback) == 'string') {
        eval(callback + '.call(' + callback + ', resp)');
      }
    },
    complete : function(xhr, status, sta) {
      if (status == 'error' || status == 'parsererror') {
        alert("Could not get gateway configuration.");
      }
    },
    dataType: 'json'
  };
  
  $.ajax(ajaxOptions);
}

/**
 * Make status calls to server for transaction updates
 */
Webpayment.serverCall = function(op, callback) {
  Webpayment.config.form_build_id = $("#webpayment-payment-form input[name='form_build_id']").val();
  
  var ajaxOptions = {
    type : "POST",
    url : Drupal.settings.webpayment.statusPath,
    data : $.extend({}, Webpayment.config, {'op' : op}),
    success : function(resp, status) {
      if (typeof(callback) == 'function') {
        callback.call(callback, resp, status);
      }
    },
    complete : function(xhr, status, sta) {
      if (status == 'error') {
        alert("Error.");
      }
      else if (status == 'parsererror') {
        alert("Parser error.");
      }
    },
    dataType: 'json'
  };
  $.ajax(ajaxOptions);
} 

/**
 * Create a new transaction and return the transaction information
 *
 */
Webpayment.newtx = function(url, title, done) {
  // Do nothing if there is a transaction ongoing already
  if (!Webpayment.lock) {
    // Maintain a lock to prevent initiating another transaction while this is ongoing
    Webpayment.lock = true;
    
  //  url = 'http://localhost/websms/site/webpayment/pay/confirm?txnref=79433208&mertid=ZIB030010000148&rsp=00&rspcode=00&rspdesc=Approved%20by%20Financial%20Institution&paytxncode=000000046701&amt=36540&fee=0&dtime=18/May/2010%2018:25:23';
    Webpayment.serverCall('init', function (resp, status) {
        $('#status-messages').html(resp.status_messages);
        $('#webpayment-txid').html(resp.data.txid);
        Webpayment.config.txid = resp.data.txid
        Webpayment.open(url, title, done);
    });
  }
}

/**
 * Create a new popup for webpay and add it to the DOM
 *
 * @return popup object
 */
Webpayment.open = function(url, title, done) {
  var body = '<iframe id="webpayment-iframe" style="width:100%;height:' + Drupal.settings.webpayment.iframe.height + 'px"></iframe>';
  var buttons = '';
  
//  Popups.addLoading();
  Webpayment.popup = Popups.open(null, title, body, buttons, Drupal.settings.webpayment.iframe.width);
  
  // Remove popups' default click-to-close action...
  Webpayment.popup.$popupClose().unbind('click');
  
  // ...substitute ours so our function is called first
  Webpayment.popup.$popupClose().click(function () { Webpayment.closeAction(done); });
  
  // Popups closes windows when overlay is clicked, we don't want this so we remove it
  $('#popups-overlay').unbind('click');
  
  if (typeof(url) == 'function') {
    url = url.call(Webpayment, Webpayment.config.txid, Webpayment.config.amount)
  }
  else if (typeof(url) == 'string') {
    // A callback may be passed in as a string. Edge case, test & confirm this
    try {
      if (typeof(eval(url)) == 'function') {
        url = eval(url).call(Webpayment, Webpayment.config.txid, Webpayment.config.amount)
      }
    }
    catch (e) {
      // Do nothing
    }
  }
  else {
    // No url, abort
    return undefined;
  }
  
  // Load url
  $("#webpayment-iframe").attr('src', url);
//  Webpayment.bindReady($('#webpayment-iframe').get(0));

};

/**
 * Function called when the popup has been closed
 *
 * This will query the status of the transaction and update transaction logs
 */
Webpayment.popupdone = function(callback) {
  // Check the status of the transaction and update the transaction log accordingly (server-side)
  Webpayment.serverCall('status', function(resp, status) {
      // Display messages
      $('#status-messages').html(resp.status_messages);
      delete resp.status_messages;
      
      $('#webpayment_message').html(resp.message);

      Webpayment.updateStatus(resp.data);
      
      if (typeof(callback) == 'function') {
        callback.call(callback, resp);
      }
      else if (typeof(callback) == 'string') {
        eval(callback + '.call(' + callback + ', resp)');
      }
      
      // Redirect successful transaction to redirect url
//      location.href = Drupal.settings.webpayment.successUrl + '/' + resp.data.txid;
    });
    
  var complete = function(xhr, status, sta) {
      if (status == 'error' || status == 'parsererror') {
        alert("Could not retrieve transaction record.");
      }
    };
};

/**
 * Webpayment utility function to handle different transaction statuses
 */
Webpayment.updateStatus = function(transaction) {
  // Re-enable disable button
  Webpayment.lock = false;
  
  switch(transaction.status.status) {
    case 'success':
      // Remove the click event from the button so it can't repeat the payment
      $('#edit-paybutton').unbind('click').attr('onclick', '').val('Complete Payment');
      $('#edit-cancel').remove();
      $('#edit-back').remove();
      break;
    case 'processing':
      // Change button to Continue
      $('#edit-paybutton').unbind('click').attr('onclick', '').val('Continue');
      $('#edit-cancel').remove();
      $('#edit-back').remove();
      break;
    case 'notrans':
      // Change button to try again
      $('#edit-paybutton').unbind('click').attr('onclick', '').bind('click', function(){doPayment(); return false;}).val('Try again!');
      $('#edit-cancel').show();
      $('#edit-back').show();
      break;
    case 'fail':
      // Change button to try again
      $('#edit-paybutton').unbind('click').attr('onclick', '').bind('click', function(){doPayment(); return false;}).val('Try again!');
      $('#edit-cancel').show();
      $('#edit-back').show();
      break;
    case 'pending':
    case 'error':
      // Change button to refresh transaction record
      $('#edit-paybutton').unbind('click').attr('onclick', '').bind('click', function(){Webpayment.popupdone(null); return false}).val('Refresh');
      $('#edit-cancel').hide();
      $('#edit-back').hide();
      break;
  }  
}

Webpayment.ready = function(e) {
  var $doc = (this.contentDocument) ? this.contentDocument : this.Document;

  try {
    $('#btnClose', $doc).click(Webpayment.closeAction);
  }
  catch (e) {
    if (console) console.log('Webpayment.module: ' + e);
  }
  
  try {
    $doc.window.close = Webpayment.closeAction;
  }
  catch (e) {
    if (console) console.log('Webpayment.module: ' + e);
  }
};

Webpayment.bindReady = function($iframe) {
  var $doc = ($iframe.contentDocument) ? $iframe.contentDocument : $iframe.Document;
  // Mozilla, Opera and webkit nightlies currently support this event
  if (document.addEventListener) {
    // Use the handy event callback
    $doc.addEventListener( "DOMContentLoaded", Webpayment.ready, false );
    
    // A fallback to window.onload, that will always work
    $iframe.addEventListener( "load", Webpayment.ready, false );
  } 

  // If IE event model is used
  else if (document.attachEvent) {
    // ensure firing before onload,
    // maybe late but safe also for iframes
    $doc.attachEvent("onreadystatechange", Webpayment.ready);
    
    // A fallback to window.onload, that will always work
    $iframe.attachEvent( "onload", Webpayment.ready );
  }
};

