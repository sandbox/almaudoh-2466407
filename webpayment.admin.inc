<?php
//$Id$

/**
 * @page
 *  
 * Administrative pages for webpayment module
 */

 /*
 * Webpayment Administration - Settings for gateways
 */

/*
 * Form for setting which gateways are available
 */
function webpayment_admin_enable_form() {
  $gateways = webpayment_gateways();
  
  if (empty($gateways)) drupal_set_message(t('No payment gateway modules installed. Please install them at the !link page.', array('!link' => l('modules', 'admin/build/modules'))), 'warning');
  
  $enabled = variable_get('webpayment_enabled_gateways', array());
  
  foreach ($gateways as $identifier => $gateway) {
    $options[$identifier] = '';
    $form[$gateway['name']]['id'] = array('#title' => $gateway['title'], '#value' => $identifier);
    if (function_exists($gateway['configure form'])) {
      $form[$gateway['name']]['configure'] = array('#value' => l(t('configure'), 'admin/webpayment/gateways/'. $identifier));
    }
    else {
      $form[$gateway['name']]['configure'] = array('#value' => l(t('configure'), 'admin/webpayment/gateways/'. $identifier));
//      $form[$gateway['name']]['configure'] = array('#value' => t('No configuration options'));
    }
  }

  $form['default'] = array('#type' => 'checkboxes', '#options' => $options, '#default_value' => $enabled);
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
  );
  
  return $form;
}

function webpayment_admin_enable_form_submit($form, &$form_state) {
  // Process form submission to set enabled gateways
//  dpm($form_state);
  if ($form_state['values']['default']) {
    drupal_set_message(t('Enabled gateways updated.'));
    variable_set('webpayment_enabled_gateways', $form_state['values']['default']);
  }
}

function theme_webpayment_admin_enable_form($form) {
//  dpm($form);
  $rows = array();
  foreach ($form as $name => $element) {
    if (isset($element['id']) && is_array($element['id'])) {
      $rows[] = array(
        drupal_render($form['default'][$element['id']['#value']]),
        check_plain(''),
        check_plain($element['id']['#title']),
        drupal_render($element['configure']),
      );
      unset($form[$name]);
    }
  }
  $header = array(t('Enable'), t('') /* Logo */, t('Name'), array('data' => t('Operations'), 'colspan' => 1));
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}


/**
 * Form to configure a gateway option
 */
function webpayment_admin_gateway_form(&$form_state, $gateway_id) {
  $gateway = webpayment_gateways('gateway', $gateway_id);
  if ($gateway && function_exists($gateway['configure form'])) {
    drupal_set_title(t('@gateway configuration', array('@gateway' => $gateway['title'])));
    $form = $gateway['configure form']($gateway['configuration']);
    
  }
  
  $form['charges'] = array(
    '#type' => 'fieldset',
    '#title' => t('Gateway charges'),
    '#description' => t('Specify gateway charges here.'),
    '#weight' => -10,
  );

  $form['charges']['charge'] = array(
    '#type' => 'textfield',
    '#title' => t('Gateway charge'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $gateway['configuration']['charge'],
  );

  $form['charges']['unit'] = array(
    '#title' => t('Unit'),
    '#type' => 'radios',
    '#options' => array('percent' => 'Percent (%)', 'amount' => 'Amount (NGN)'),
    '#default_value' => isset($gateway['configuration']['unit']) ? $gateway['configuration']['unit'] : 'percent',
  );

  $form['charges']['min'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $gateway['configuration']['min'],
  );

  $form['charges']['max'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $gateway['configuration']['max'],
  );

  $form['limits'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment limits'),
    '#description' => t('Limits to payments allowed here.'),
    '#collapsible' => TRUE,
    '#weight' => -10,
  );

  $form['limits']['minimum'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum payment'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $gateway['configuration']['minimum'],
  );

  $form['limits']['maximum'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum payment'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $gateway['configuration']['maximum'],
  );

  $form['window'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment frame dimensions'),
    '#description' => t('Dimensions for the payment window frame.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -11,
  );

  $height = !empty($gateway['configuration']['iframeheight']) ? $gateway['configuration']['iframeheight'] : WEBPAYMENT_FRAME_HEIGHT;
  $form['window']['iframeheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame height (px)'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $height,
  );

  $width = !empty($gateway['configuration']['iframewidth']) ? $gateway['configuration']['iframewidth'] : WEBPAYMENT_FRAME_WIDTH;
  $form['window']['iframewidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame width (px)'),
    '#size' => 40,
    '#maxlength' => 100,
    '#default_value' => $width,
  );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['gateway'] = array('#type' => 'value', '#value' => $gateway);

  return $form;
}

/**
 * Validation for gateway configuration. If the gateway defines a validation
 * function it uses that.
 */
function webpayment_admin_gateway_form_validate($form, &$form_state) {
  // Pass validation to gateway
  $function = $form_state['values']['gateway']['configure form'] .'_validate';
  if (function_exists($function)) {
    $function($form, $form_state);
  }
  
  if (!is_numeric($form_state['values']['charge'])) {
    form_set_error('charge', t('Specify a valid amount to charge.'));
  }
  
  if (!is_numeric($form_state['values']['min'])) {
    form_set_error('min', t('Specify a valid minimum amount to charge.'));
  }
  
  if (!is_numeric($form_state['values']['max'])) {
    form_set_error('max', t('Specify a valid maximum amount to charge.'));
  }
}

function webpayment_admin_gateway_form_submit($form, &$form_state) {
  $gateway = $form_state['values']['gateway'];
  
  // Remove uneccesary values
  unset($form_state['values']['op'], $form_state['values']['submit'], $form_state['values']['gateway'], 
      $form_state['values']['form_token'], $form_state['values']['form_id'], $form_state['values']['form_build_id']);
  variable_set('webpayment_'. $gateway['identifier'] .'_settings', $form_state['values']);
  drupal_set_message(t('The gateway settings have been saved.'));
  $form_state['redirect'] = 'admin/webpayment/gateways';
}  
