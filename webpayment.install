<?php
// $Id$

/**
 * @file
 * 
 * Installation script for webpayment 
 */
 
/**
 * Implementation of hook_schema()
 * 
 * webpayment schema consists of database table to keep track of transactions and transaction id's
 */
function webpayment_schema() {
//  Transaction date and time
//  Transaction reference number
//  Approved amount
//  Response code
//  Response description
//  Transaction amount (what is to be approved. This could be optional)
//  Customer name and ID (Optional)
//  Requery feature to confirm the status of pending transactions.

  $schema['webpayment_transaction'] = array(
  'description' => t('Stores transaction information and other metadata'),
  'fields' => array(
    'txid' => array(
      'description' => t('The unique identifier for the transaction.'),
      'type' => 'serial',
      'not null' => TRUE,
      'disp-width' => '11',
    ),
    'uid' => array(
      'description' => t('The User ID of the user who initiated the transaction.'),
      'type' => 'int',
      'not null' => TRUE,
      'unsigned' => TRUE,
      'disp-width' => '11',
    ),
    'txkey' => array(
      'description' => t('The transaction key. Used to identify the source of the transaction.'),
      'type' => 'varchar',
      'length' => 100,
      'not null' => FALSE,
    ),
    'timestamp' => array(
      'description' => t('The timestamp the transaction was initiated.'),
      'type' => 'int',
      'not null' => FALSE,
      'unsigned' => TRUE,
      'disp-width' => '11',
    ),
    'amount' => array(
      'description' => t('The amount of money requested for in the transaction.'),
      'type' => 'float',
      'not null' => TRUE,
      'unsigned' => TRUE,
      'size' => 'normal',
    ),
    'description' => array(
      'description' => t('The description of the source of the transaction.'),
      'type' => 'text',
      'size' => 'normal',
      'not null' => FALSE,
    ),
    'txamount' => array(
      'description' => t('The amount of money approved in the transaction, excluding fees charged.'),
      'type' => 'float',
      'not null' => TRUE,
      'unsigned' => TRUE,
      'size' => 'normal',
    ),
    'txcharge' => array(
      'description' => t('The sum of fees charged in the transaction.'),
      'type' => 'float',
      'not null' => TRUE,
      'unsigned' => TRUE,
      'size' => 'normal',
    ),
    'status' => array(
      'description' => t('The status of the transaction. Pending transactions may need to be requeried'),
      'type' => 'text',
      'size' => 'normal',
      'not null' => FALSE,
    ),
    'gateway' => array(
      'description' => t('The identifier of the transaction gateway.'),
      'type' => 'varchar',
      'length' => '255',
      'not null' => FALSE,
    ),
    'open' => array(
      'description' => t('Whether the transaction is still open. A boolean value that can be used to mark a transaction to prevent re-use.'),
      'type' => 'int',
      'not null' => TRUE,
      'unsigned' => TRUE,
      'disp-width' => '1',
      'default' => '1',
    ),
  ),
  'primary key' => array('txid'),
  'indexes' => array(
    'timestamp' => array('timestamp'),
    'uid' => array('uid'),
    'txkey' => array('txkey'),
    ),
  );
  
  return $schema;
}

 /**
 * Implementation of hook_install()
 */
function webpayment_install() {
  drupal_install_schema('webpayment');
  
  // Set the installation timestamp to use for generating transaction ids
  if (variable_get('webpayment_install_timestamp', 'HELLO') === 'HELLO') {
    variable_set('webpayment_install_timestamp', time());
  }
}

 /**
 * Implementation of hook_uninstall().
 */
function webpayment_uninstall() {
  $gateways = webpayment_gateways('names');
  foreach ($gateways as $gateway) {
    variable_del('webpayment_' . $gateway . '_settings');
  }
  variable_del('webpayment_install_timestamp');
  variable_del('webpayment_enabled_gateways');
  
  drupal_uninstall_schema('webpayment');
}

