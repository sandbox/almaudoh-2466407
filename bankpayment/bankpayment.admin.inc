<?php
//$Id$

/**
 * @file Administrative settings for bankpayment
 */

/**
 * Set up the webpayment form to post for vpay
 */
function bankpayment_bank_form($form_state, $account_id) {
  $defaults = bankpayment_get_account($account_id);
  $form['bank_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank Name'),
    '#description' => t('Name of bank in which account is domiciled'),
    '#required' => TRUE,
    '#default_value' => $defaults->bank_name,
  );
  $form['account_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account Name'),
    '#description' => t('Name of account in bank.'),
    '#required' => TRUE,
    '#default_value' => $defaults->account_name,
  );
  $form['account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Account Number'),
    '#required' => TRUE,
    '#default_value' => $defaults->account_number,
  );
  
  if ($defaults) {
    $form['account_id'] = array(
      '#type' => 'value',
      '#value' => $account_id,
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Set up the webpayment form to post for vpay
 */
function bankpayment_bank_form_submit(&$form, &$form_state) {
  if ($form_state['values']['account_id']) $bank = bankpayment_get_account($form_state['values']['account_id']);
  else $bank = (object) array();
  
  $bank->bank_name = $form_state['values']['bank_name'];
  $bank->account_name = $form_state['values']['account_name'];
  $bank->account_number = $form_state['values']['account_number'];
  
  bankpayment_bank_save($bank);
  $form_state['redirect'] = 'admin/webpayment/gateways/bankpayment';
}

function bankpayment_bank_save($bank) {
  if ($bank->baid) {
    $query = "UPDATE {bankpayment_accounts} SET bank_name = '%s', account_name = '%s', account_number = '%s' WHERE baid = %d";
    $args = array($bank->bank_name, $bank->account_name, $bank->account_number, $bank->baid);
  }
  else {
    $query = "INSERT INTO {bankpayment_accounts} (bank_name, account_name, account_number) VALUES  ('%s', '%s', '%s')";
    $args = array($bank->bank_name, $bank->account_name, $bank->account_number);
  }
  $result = db_query($query, $args);
}