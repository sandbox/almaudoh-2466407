<?php
// $Id: page.tpl.php,v 1.18.2.1 2009/04/30 00:13:31 goba Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php // TODO: ie styles here ?>
    <![endif]-->
    <style>
    /* Styles for this page */
    body {
      font-family: Arial, sans-serif;
      font-size: 0.9em;
      line-height: 1.5;
      background-image: url(<?php print url(drupal_get_path('module', 'bankpayment')); ?>/background_slim.png);
      background-repeat: repeat-x;
      text-align: left;
    }
    
    h2 {
      text-align: center;
    }

    div#logo-floater {
      display: none;
    }    
    
    #center .inner {
      background: #fff;
      border: 1px solid #ddd;
      margin: 5px;
      padding: 15px 5px 15px 10px;
      box-shadow: 0px 0px 3px #ccc;
      -moz-box-shadow: 0px 0px 3px #ccc;
      -webkit-box-shadow: 0px 0px 3px #ccc;
      -moz-border-radius: 5px;
      -webkit-border-radius: 5px;
    }
    
    #cards-banks {
      text-align: center;
    }
    
    #cards-banks img {
/*      float: right;*/
      margin: 0;
      display: inline;
    }
    </style>
  </head>
  <body<?php // body class; ?>>

<!-- Layout -->

    <div id="wrapper">
    <div id="container" class="clear-block">

      <div id="header">
        <div id="logo-floater">
        <?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields);
          if ($site_fields) {
            $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          }
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
            if ($logo) {
              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
        </div>

      </div> <!-- /header -->
      <div id="cards-banks">
        <img src="<?php print url(drupal_get_path('module', 'bankpayment')); ?>/banks.png" alt="diamond,uba,zenith,intercontinental,ecobank,firstbank">
      </div>

      <div id="center"><div id="squeeze"><div class="right-corner"><div class="left-corner">
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <div class="inner clearfix">
            <?php print $content ?>
          </div>
          <?php print $feed_icons ?>
          <div id="footer"><?php print $footer_message . $footer ?></div>
      </div></div></div></div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->

    </div> <!-- /container -->
  </div>
<!-- /layout -->

  <?php print $closure ?>
  </body>
</html>
