<?php
  
/** Bankpayment Approval Workflow */
function bankpayment_payment_page($txid, $txamt, $curr) {
  return drupal_get_form('bankpayment_payment_form', $txid, $txamt);
}

// Later we may want to look at crediting on behalf of others
function bankpayment_payment_form($form_state, $txid, $txamt) {
  $banks = bankpayment_get_account();
  
  foreach ($banks as $account_id => $info) {
    $options[$account_id] = $info->bank_name . ' - ' . $info->account_number;
  }
  
  $form = array(
    'txid_display' => array(
      '#type' => 'item',
      '#title' => t('Transaction number'),
      '#value' => '# ' . $txid,
    ),
    /*'username' => array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#size' => 40,
      '#default_value' => $GLOBALS['user']->name,
      '#autocomplete_path' => 'user/autocomplete',
    ),*/
    'baid' => array(
      '#type' => 'select',
      '#title' => t('Account'),
      '#description' => t('Select the account where deposit was made'),
      '#options' => $options,
      '#required' => TRUE,
    ),
    'depositor' => array(
      '#type' => 'textfield',
      '#title' => t('Depositor name'),
      '#required' => TRUE,
      '#size' => 40,
    ),
    'slip_number' => array(
      '#type' => 'textfield',
      '#title' => t('Deposit slip number'),
      '#description' => t('Put in the deposit slip number here'),
      '#required' => TRUE,
      '#size' => 40,
    ),
    'txamount' => array(
      '#type' => 'item',
      '#title' => t('Amount Requested'),
      '#value' => _bankpayment_format_currency($txamt),
    ),
    'amount' => array(
      '#type' => 'textfield',
      '#title' => t('Amount Paid'),
      '#description' => t('Enter the amount that was paid'),
      '#default_value' => $txamt,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#size' => 40,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    ),
    'txid' => array(
      '#type' => 'value',
      '#value' => $txid,
    ),
    'txamt' => array(
      '#type' => 'value',
      '#value' => $txamt,
    ),
  );
  
  return $form;
}

function bankpayment_payment_form_submit(&$form, &$form_state) {
  bankpayment_workflow_update('initiate', $form_state['values']);
  drupal_goto('webpayment/pay/confirm/' . $form_state['values']['txid']);
}

/**
 * Workflow management forms and pages
 */
 
/**
 * Display all bankpayment requests and status
 */
function bankpayment_request_list() {
  // Get all transactions
  $btxns = array();
  $tabledata = array();
  $banks = bankpayment_get_account();
  $statuses = bankpayment_payment_status();
  $can_review = user_access('review bankpayment request');
  $can_verify = user_access('verify bankpayment request');
  $can_approve = user_access('approve bankpayment request');
  $result = db_query('SELECT * from {bankpayment_transactions}');
  while ($row = db_fetch_object($result)) {
    $row->user = user_load(array('uid' => $row->uid));
    $btxns[] = $row;
    switch ($row->status) {
      case BANKPAYMENT_STATUS_REQUESTED:
        $link = $can_review ? l('Review', 'admin/webpayment/gateways/bankpayment/requests/' . $row->btid . '/review') : '';
        break;
        
      case BANKPAYMENT_STATUS_REVIEWED:
        $link = $can_verify ? l('Verify', 'admin/webpayment/gateways/bankpayment/requests/' . $row->btid . '/verify') : '';
        break;
        
      case BANKPAYMENT_STATUS_VERIFIED:
        $link = $can_approve ? l('Approve', 'admin/webpayment/gateways/bankpayment/requests/' . $row->btid . '/approve') : '';
        break;
        
      case BANKPAYMENT_STATUS_APPROVED:
        $link = $can_approve ? l('Close', 'admin/webpayment/gateways/bankpayment/requests/' . $row->btid . '/close') : '';
        break;
        
      case BANKPAYMENT_STATUS_REJECTED:
      default:
        $link = '';
        break;
    }
    $tabledata[] = array(
      $row->txid, 
      $banks[$row->baid]->bank_name . ' - ' . $banks[$row->baid]->account_number, 
      _bankpayment_format_currency($row->amount),
      $statuses[$row->status], 
      $link
    );
  }
  
  return theme('table', array(t('Transaction No'), t('Account No'), t('Amount'), t('Status'), ''), $tabledata, array(), t('Bankpayment Transactions'));
}

function bankpayment_workflow_form($form_state, $btid, $action) {
  $btxn = db_fetch_object(db_query('SELECT * from {bankpayment_transactions} WHERE btid = %d', $btid));
  if (empty($btxn)) { 
    drupal_not_found(); 
    return;
  }
  
  $banks = bankpayment_get_account();
  foreach ($banks as $account_id => $info) {
    $options[$account_id] = $info->bank_name . ' - ' . $info->account_number;
  }
  
  $statuses = bankpayment_payment_status();
  $txn = webpayment_transaction_load($btxn->txid);
  
  if ($action == 'review') {
    $form['txid_display'] = array(
      '#type' => 'item',
      '#title' => t('Transaction number'),
      '#value' => '# ' . $btxn->txid,
    );
    $form['status'] = array(
      '#type' => 'item',
      '#title' => t('Transaction request status'),
      '#value' => $statuses[$btxn->status],
    );
    /*$form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#size' => 40,
      '#default_value' => $GLOBALS['user']->name,
      '#autocomplete_path' => 'user/autocomplete',
    );*/
    
    $form['baid'] = array(
      '#type' => 'select',
      '#title' => t('Account'),
      '#description' => t('Account where deposit was made'),
      '#options' => $options,
      '#default_value' => $btxn->baid,
      '#required' => TRUE,
    );
    
    $form['depositor'] = array(
      '#type' => 'textfield',
      '#title' => t('Depositor name'),
      '#default_value' => $btxn->depositor,
      '#required' => TRUE,
      '#size' => 40,
    );
    
    $form['slip_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Deposit slip number'),
      '#default_value' => $btxn->slip_number,
      '#required' => TRUE,
      '#size' => 40,
    );
    
    $form['txamount'] = array(
      '#type' => 'item',
      '#title' => t('Transaction Amount'),
      '#value' => _bankpayment_format_currency($txn->amount),
    );
    
    $form['amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Amount Paid'),
      '#default_value' => $btxn->amount,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#size' => 40,
    );
  }
  
  else {   
    $form['txid_display'] = array(
      '#type' => 'item',
      '#title' => t('Transaction number'),
      '#value' => '# ' . $btxn->txid,
    );
    /*$form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#size' => 40,
      '#default_value' => $GLOBALS['user']->name,
      '#autocomplete_path' => 'user/autocomplete',
    );*/
    
    $form['baid'] = array(
      '#type' => 'item',
      '#title' => t('Account'),
      '#value' => $banks[$btxn->baid]->bank_name . ' - ' . $banks[$btxn->baid]->account_number ,
    );
    
    $form['depositor'] = array(
      '#type' => 'item',
      '#title' => t('Depositor name'),
      '#value' => $btxn->depositor,
    );
    
    $form['slip_number'] = array(
      '#type' => 'item',
      '#title' => t('Deposit slip number'),
      '#value' => $btxn->slip_number,
    );
    
    $form['txamount'] = array(
      '#type' => 'item',
      '#title' => t('Transaction Amount'),
      '#value' => _bankpayment_format_currency($txn->amount),
    );
    
    $form['amount'] = array(
      '#type' => 'item',
      '#title' => t('Amount Paid'),
      '#value' => _bankpayment_format_currency($btxn->amount),
    );
  }
  
  $form['btid'] = array(
    '#type' => 'value',
    '#value' => $btxn->btid,
  );
  
  $form['action'] = array(
    '#type' => 'value',
    '#value' => $action,
  );
  
  switch ($action) {
    case 'review':
      $caption = t('Accept');
      break;
    case 'verify':
      $caption = t('Verify');
      break;
    case 'approve':
      $caption = t('Approve');
      break;
    case 'close':
      $caption = t('Close');
      break;
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $caption,
  );

  $form['reject'] = array(
    '#type' => 'submit',
    '#value' => t('Reject'),
  );

  return $form;
}

function bankpayment_workflow_form_submit(&$form, &$form_state) {
  if ($form_state['values']['op'] == t('Reject')) {
    bankpayment_workflow_update('reject', $form_state['values']);
    drupal_set_message(t('Transaction request for transaction #@txid has been rejected.', array('@txid' => $form_state['values']['txid'])));
  }
  
  else if ($form_state['values']['action'] == 'review'  && $form_state['values']['op'] == t('Accept')) {
    bankpayment_workflow_update('review', $form_state['values']);
    drupal_set_message(t('Transaction request for transaction #@txid has been reviewed.', array('@txid' => $form_state['values']['txid'])));
  }
  
  else if ($form_state['values']['action'] == 'verify'  && $form_state['values']['op'] == t('Verify')) {
    bankpayment_workflow_update('verify', $form_state['values']);
    drupal_set_message(t('Transaction request for transaction #@txid has been verified.', array('@txid' => $form_state['values']['txid'])));
  }
  
  else if ($form_state['values']['action'] == 'approve' && $form_state['values']['op'] == t('Approve')) {
    bankpayment_workflow_update('approve', $form_state['values']);
    drupal_set_message(t('Transaction request for transaction #@txid has been approved.', array('@txid' => $form_state['values']['txid'])));
  }
  
  else if ($form_state['values']['action'] == 'close'   && $form_state['values']['op'] == t('Close')) {
    bankpayment_workflow_update('close', $form_state['values']);
    drupal_set_message(t('Transaction request for transaction #@txid has been closed.', array('@txid' => $form_state['values']['txid'])));
  }
  drupal_goto('http://pluralsms.mac/admin/webpayment/gateways/bankpayment/requests');
}

/**
 * Step forward in the workflow by updating the specified record
 */
function bankpayment_workflow_update($action, $info) {
  static $statuses;
  if (!isset($statuses)) {
    $statuses = bankpayment_payment_status();
  }
  
  if ($info['btid']) {
    $btxn = db_fetch_object(db_query('SELECT * from {bankpayment_transactions} WHERE btid = %d', $info['btid']));
    $btxn->user = user_load(array('uid' => $btxn->uid));
  }
  if ($info['baid']) {
    $bank = bankpayment_get_account($info['baid']);
  }
  switch($action) {
    case 'initiate':
      if ($bank) {
//        $owner = user_load(array('name' => $info['username'])); 
        $owner = $GLOBALS['user'];
        $result = db_query("INSERT INTO {bankpayment_transactions} (txid, uid, baid, timestamp, depositor, slip_number, amount, status) "
                          ."VALUES (%d, %d, %d, %d, '%s', '%s', %d, %d)", $info['txid'], $owner->uid, $bank->baid, time(),
                          $info['depositor'], $info['slip_number'], $info['amount'], BANKPAYMENT_STATUS_REQUESTED);
        if ($result) {
          $btid = db_last_insert_id('bankpayment_transactions', 'btid');
          $arguments = array(
            '@user_1' => $GLOBALS['user']->name,
//            '@user_2' => $info['username'],
            '@user_2' => $GLOBALS['user']->name,
            '@time' => format_date(time()),
            '@account' => $bank->bank_name . ' - ' . $bank->account_number,
            '@btid' => $btid,
            '@depositor' => $info['depositor'],
            '@slip_number' => $info['slip_number'],
            '@amount' => _bankpayment_format_currency($info['amount'], 'NGN'),
            '@txid' => $info['txid'],
          );
          bankpayment_workflow_log('@time: @user_1: Initiated new bankpayment request #@btid for <b>@user_2</b>. Account: @account; Depositor: @depositor; Slip: @slip_number; Amount: @amount; Transaction: @txid',
              $arguments, $btid);
        }
      }
      break;
      
    case 'review':
      if ($btxn) {
        if ($info['column']) {
          if (in_array($info['column'], array('uid', 'timestamp', 'amount', 'status'))) $placeholder = '%d';
          else $placeholder = "'%s'";
          $result = db_query("UPDATE {bankpayment_transactions} SET {$info['column']} = $placeholder, status = %d WHERE btid = %d", $info['new_value'], BANKPAYMENT_STATUS_REVIEWED, $btxn->btid);
        }
        else {
          $result = db_query("UPDATE {bankpayment_transactions} SET status = %d WHERE btid = %d", BANKPAYMENT_STATUS_REVIEWED, $btxn->btid);
        }
        if ($result) {
          $arguments = array(
            '@user_1' => $GLOBALS['user']->name,
            '@user_2' => $btxn->user->name,
            '@time' => format_date(time()),
            '@btid' => $btxn->btid,
            '@column' => $info['column'],
            '@old_value' => ($info['column'] ? $btxn->{$info['column']} : '<unknown>'),
            '@new_value' => $info['new_value'],
          );
          bankpayment_workflow_log('@time: @user_1: Reviewed bankpayment request #@btid for <b>@user_2</b>. Updated @column from <b>"@old_value"</b> to <b>"@new_value"</b>.',
              $arguments, $info['btid']);
        }
      }
      break;
      
    case 'verify':
      if ($btxn) {
        $result = db_query("UPDATE {bankpayment_transactions} SET status = %d WHERE btid = %d", BANKPAYMENT_STATUS_VERIFIED, $btxn->btid);
        if ($result) {
          $arguments = array(
            '@user_1' => $GLOBALS['user']->name,
            '@user_2' => $btxn->user->name,
            '@time' => format_date(time()),
            '@btid' => $btxn->btid,
            '@old_status' => $statuses[$btxn->status],
            '@new_status' => $statuses[BANKPAYMENT_STATUS_VERIFIED],
          );
          bankpayment_workflow_log('@time: @user_1: Verified bankpayment request #@btid for <b>@user_2</b>. Status changed from <b>@old_status</b> to <b>@new_status</b>.',
              $arguments, $info['btid']);
        }
      }
      break;
      
    case 'reject':
      if ($btxn) {
        $result = db_query("UPDATE {bankpayment_transactions} SET status = %d WHERE btid = %d", BANKPAYMENT_STATUS_REJECTED, $btxn->btid);
        if ($result) {
          $arguments = array(
            '@user_1' => $GLOBALS['user']->name,
            '@user_2' => $btxn->user->name,
            '@time' => format_date(time()),
            '@btid' => $btxn->btid,
            '@old_status' => $statuses[$btxn->status],
            '@new_status' => $statuses[BANKPAYMENT_STATUS_REJECTED],
          );
          bankpayment_workflow_log('@time: @user_1: Rejected bankpayment request #@btid for <b>@user_2</b>. Status changed from <b>@old_status</b> to <b>@new_status</b>.',
              $arguments, $info['btid']);
        }
      }
      break;
      
    case 'approve':
      if ($btxn) {
        $result = db_query("UPDATE {bankpayment_transactions} SET status = %d WHERE btid = %d", BANKPAYMENT_STATUS_APPROVED, $btxn->btid);
        if ($result) {
          $arguments = array(
            '@user_1' => $GLOBALS['user']->name,
            '@user_2' => $btxn->user->name,
            '@time' => format_date(time()),
            '@btid' => $btxn->btid,
            '@old_status' => $statuses[$btxn->status],
            '@new_status' => $statuses[BANKPAYMENT_STATUS_APPROVED],
          );
          bankpayment_workflow_log('@time: @user_1: Approved bankpayment request #@btid for <b>@user_2</b>. Status changed from <b>@old_status</b> to <b>@new_status</b>.',
              $arguments, $info['btid']);
        }
      }
      break;
      
  }
}

function bankpayment_workflow_log($message, $arguments, $btid) {
  global $user;
  $result = db_query("INSERT INTO {bankpayment_log} (btid, uid, message) VALUES (%d, %d, '%s')", $btid, $user->uid, t($message, $arguments));
}

function _bankpayment_format_currency($amount, $curr='NGN') {
  return $curr . number_format($amount, 2, '.', ',');
}

