<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>
      <?php print $title ?>
    </title>
    <?php print $scripts ?>
    <?php print $styles ?>
  </head>
  <body>
    <div id="webpayment-content" class="content webpayment-center">
      <?php print $messages; ?>
      <div>
        Redirecting....
      </div>
      <div>
        <?php print $form; ?>
      </div>
    </div>
    <script>
      // Submit form immediately page loads
      $(document).ready(function() {
        $('form').submit();
      });
    </script>
  </body>
</html>

