<?php
// $Id$

function webpayment_views_default_views() {
  // Insert the exported view for webpayment transactions here
  $view = new view;
  $view->name = 'transactions';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'webpayment_transaction';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'uid' => array(
      'id' => 'uid',
      'table' => 'webpayment_transaction',
      'field' => 'uid',
    ),
  ));
  $handler->override_option('fields', array(
    'counter' => array(
      'label' => 'S/No',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'counter',
      'table' => 'views',
      'field' => 'counter',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'txid' => array(
      'label' => 'Transaxn ID',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'admin/settings/transactions/[txid]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => '',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'txid',
      'table' => 'webpayment_transaction',
      'field' => 'txid',
      'relationship' => 'none',
    ),
    'timestamp' => array(
      'label' => 'Date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'timestamp',
      'table' => 'webpayment_transaction',
      'field' => 'timestamp',
      'relationship' => 'none',
    ),
    'description' => array(
      'label' => 'Description',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'description',
      'table' => 'webpayment_transaction',
      'field' => 'description',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount (NGN)',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'set_precision' => 1,
      'precision' => '2',
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 1,
      'id' => 'amount',
      'table' => 'webpayment_transaction',
      'field' => 'amount',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'txamount' => array(
      'label' => 'Apprvd Amt(NGN)',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'set_precision' => 1,
      'precision' => '2',
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'txamount',
      'table' => 'webpayment_transaction',
      'field' => 'txamount',
      'relationship' => 'none',
    ),
    'txcharge' => array(
      'label' => 'Charge (NGN)',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'set_precision' => 1,
      'precision' => '2',
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 1,
      'id' => 'txcharge',
      'table' => 'webpayment_transaction',
      'field' => 'txcharge',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'name' => array(
      'label' => 'User',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
      'anonymous_text' => '',
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'uid',
    ),
    'status' => array(
      'label' => 'Status',
      'accessor' => 'error',
      'exclude' => 0,
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'id' => 'status',
      'table' => 'webpayment_transaction',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'open' => array(
      'label' => 'Open',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'type' => 'true-false',
      'not' => 0,
      'exclude' => 1,
      'id' => 'open',
      'table' => 'webpayment_transaction',
      'field' => 'open',
      'relationship' => 'none',
    ),
    'gateway' => array(
      'label' => 'Gateway',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'gateway',
      'table' => 'webpayment_transaction',
      'field' => 'gateway',
      'relationship' => 'none',
    ),
    'txkey' => array(
      'label' => 'Key',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 1,
      'id' => 'txkey',
      'table' => 'webpayment_transaction',
      'field' => 'txkey',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'administer web payment',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Webpayment Transactions');
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 0);
  $handler->override_option('items_per_page', 20);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'desc',
    'columns' => array(
      'counter' => 'counter',
      'txid' => 'txid',
      'description' => 'description',
      'amount' => 'amount',
      'txamount' => 'txamount',
      'txcharge' => 'txcharge',
      'name' => 'name',
      'timestamp' => 'timestamp',
      'status' => 'status',
      'open' => 'open',
      'gateway' => 'gateway',
      'txkey' => 'txkey',
    ),
    'info' => array(
      'counter' => array(
        'separator' => 'x2',
      ),
      'txid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'description' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'txamount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'txcharge' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'timestamp' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'open' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'gateway' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'txkey' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'timestamp',
  ));
  $handler = $view->new_display('page', 'Admin Page', 'page_1');
  $handler->override_option('path', 'admin/reports/transactions');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Webpayment Transactions',
    'description' => 'Transactions carried out with webpayment module',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'User Page', 'page_3');
  $handler->override_option('filters', array(
    'uid_current' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'uid_current',
      'table' => 'users',
      'field' => 'uid_current',
      'relationship' => 'uid',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
    'txkey' => array(
      'operator' => '=',
      'value' => 'credit_purchase',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'txkey',
      'table' => 'webpayment_transaction',
      'field' => 'txkey',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'access web payment',
  ));
  $handler->override_option('path', 'webpayment/transactions');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'My Transactions',
    'description' => 'View of all my transactions',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => '0',
  ));
  $handler = $view->new_display('page', 'Successful Page', 'page_5');
  $handler->override_option('filters', array(
    'txamount' => array(
      'operator' => '>',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'txamount',
      'table' => 'webpayment_transaction',
      'field' => 'txamount',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
    'uid_current' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'uid_current',
      'table' => 'users',
      'field' => 'uid_current',
      'relationship' => 'uid',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
    'txkey' => array(
      'operator' => '=',
      'value' => 'credit_purchase',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'txkey',
      'table' => 'webpayment_transaction',
      'field' => 'txkey',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'access web payment',
  ));
  $handler->override_option('path', 'webpayment/transactions/successful');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Successful Transactions',
    'description' => 'Successful transactions',
    'weight' => '1',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'Pending Page', 'page_2');
  $handler->override_option('filters', array(
    'uid_current' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'uid_current',
      'table' => 'users',
      'field' => 'uid_current',
      'relationship' => 'uid',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
    'txkey' => array(
      'operator' => '=',
      'value' => 'credit_purchase',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'txkey',
      'table' => 'webpayment_transaction',
      'field' => 'txkey',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'status' => array(
      'operator' => 'contains',
      'value' => 's:6:"status";s:7:"pending";',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'status',
      'table' => 'webpayment_transaction',
      'field' => 'status',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'access web payment',
  ));
  $handler->override_option('path', 'webpayment/transactions/pending');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Pending Transactions',
    'description' => 'Pending transactions',
    'weight' => '3',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'Admin Successful Page', 'page_4');
  $handler->override_option('filters', array(
    'txamount' => array(
      'operator' => '>',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'txamount',
      'table' => 'webpayment_transaction',
      'field' => 'txamount',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('path', 'admin/reports/transactions/successful');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Successful',
    'description' => 'Successful transactions',
    'weight' => '1',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'Admin Pending Page', 'page_6');
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => 'contains',
      'value' => 's:6:"status";s:7:"pending";',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'case' => 1,
      'id' => 'status',
      'table' => 'webpayment_transaction',
      'field' => 'status',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('path', 'admin/reports/transactions/pending');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Pending',
    'description' => 'Pending transactions',
    'weight' => '3',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;
  
  return $views;
}

