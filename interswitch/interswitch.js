Interswitch = {
  config: null
};

Interswitch.configure = function() {
  // Get config from server and save here
  Webpayment.getConfig(function(config) {
      Interswitch.config = {
        'cadpid' : config.card_partner_id,
        'mertid' : config.merchant_id,
        'redirect' : config.redirect_url
      };
  });
};

Interswitch.url = function(txid, amount) {  
  var url = 'https://webpay.interswitchng.com/webpay/purchase.aspx';
//  var url = 'https://testwebpay.interswitchng.com/webpay_pilot/purchase.aspx';
  url += '?CADPID=' + Interswitch.config.cadpid + '&MERTID=' + Interswitch.config.mertid + '&TXNREF=' + txid 
      + '&AMT=' + (amount * 100) + '&TRANTYPE=00&ECHO=&REDIRECTURL=' + Interswitch.config.redirect;

  return url;
};  
