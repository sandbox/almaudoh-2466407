<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>
      <?php print $title ?>
    </title>
    <?php print $styles ?>
    <script>
      function closeDialog() {
        if (typeof(Webpayment) == 'undefined') {
          Webpayment = window.parent.Webpayment || window.parent.parent.Webpayment || window.top.Webpayment;
        }
        if (typeof(Webpayment) == 'undefined') {
          alert('Could not close window from button. Please use the close link above.');
        }
        else {
          Webpayment.closeAction(null);
        }
      }
    </script>
  </head>
  <body>
    <div id="webpayment-content" class="content webpayment-center">
      <?php //print $messages; ?>
      <div class="webpayment-node-title"><div style="float:right;text-align:right;padding-top:4px"><?php print $transaction->logo ?></div><div><?php print $transaction->gateway; ?> Transaction Status</div></div>
      <div id="topbanner"><div>
        <?php 
        if ($transaction) {
          print t('Your transaction through <b>@gateway</b> has been concluded. Kindly review the messages below and respond as appropriate.',
                  array('@gateway' => $transaction->gateway));
        }
        else {
          print t('Your transaction could not be retrieved. Kindly review the messages below and respond as appropriate.',
                  array('@gateway' => $transaction->gateway));
        }
        ?></div>
      </div>
      <div id="webpayment-message">
        <div id="message-box" class="<?php print $status_class ?>">
         <div class="webpay-message" style="font-style:italic;font-weight:bold;margin-top: 0;">
           <?php print $message ?>
         </div>
         <div class="webpay-error" style="font-weight:bold;font-size:0.9em;color:#A22;">
           <?php print $error ?>
         </div>
         <div class="webpay-instruction" style="color:#333;">
           <?php print $instruction ?>
         </div>
        </div>
      </div>
      
      <div class="webpayment-submit">
      <form>
        <div><?php print t('Click on the button to close this dialog and continue.') ?></div>
        <div><input type="button" onclick="closeDialog()" class="form-submit" id="close" value="Close"/></div>
      </form>
      </div>
    </div>
  </body>
</html>

