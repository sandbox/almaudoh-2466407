eTranzact = {
  config: null
};

eTranzact.configure = function(config) {
  // Save config
  eTranzact.config = {
    'termid' : config[0],
    'description' : config[1]
  };
};

eTranzact.url = function(txid, amount) {  
  // @TODO need to allow SSL here later
  var url = Drupal.settings.etranzact.baseurl;
  url += '?TERMINAL_ID=' + eTranzact.config.termid + '&TRANSACTION_ID=' + txid + '&AMOUNT=' + amount + '&DESCRIPTION=' 
      + eTranzact.config.description + '&RESPONSE_URL=' + eTranzact.config.responseurl + '&LOGO_URL=' + eTranzact.config.logourl;
  return url;
//  Webpayment.open(url, eTranzact.config.title, eTranzact.config, 'eTranzact.done');
};  
