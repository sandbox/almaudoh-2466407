<?php
//$Id$

/**
 * @file Module for implementing etranzact webpay functionality
 * 
 */

/**
 * Implementation of hook_webpayment_info()
 * 
 * @return array(
 *   '<MODULENAME>' => array(  // The identifier should be the same as the module name
 *     'name' => 'Name',          // Visible on page titles and other areas. Translatable
 *     'title' => 'Admin Title',  // Title on admin configuration page. Translatable
 *     'logo' => url(drupal_get_path('module', 'etranzact') . 'images/etranzact.jpg'),  // Url to logo
 *     'method' => 'link',   // The method of calling the payment gateway. 'link' : external link, 'api' : an api call
 *     'configure form' => 'etranzact_configure_form', // Callback function for configuration form
 *   ),
 * )
 * Provide information on the webpayment module / gateway
 */
function etranzact_webpayment_info() {
  return array(
    'etranzact' => array(   // The identifier should be the same as the module name
      'name' => 'eTranzact',
      'title' => 'eTranzact Card',
      'logo' => theme('webpayment_gateway_logo', url(drupal_get_path('module', 'etranzact') . '/logo-small.jpg')),
      'method' => 'link',   // The method of calling the payment gateway. 'link' : external link, 'api' : an api call
      'configure form' => 'etranzact_configure_form', // Return form for configuration
    ),
  );
}

/**
 * Implementation of hook_jsconfig()
 * 
 * Provide configuration information for clientside javascript
 * 
 * Use this hook to add css and javascript to your page (drupal_add_js and drupal_add_css)
 * 
 * @return array with 'url callback' as the key pointing to the javascript callback that will return the payment url
 */
function etranzact_webpayment_jsconfig() {
  return array(
    'url callback' => 'eTranzact.url',
  );
}

/**
 * Implementation of hook_webpayment_javascript
 * 
 * Add javascript that you might need for your implementation here 
 * Use this hook to add css and javascript to your page (drupal_add_js and drupal_add_css)
 * @param $config Configuration options for this gateway
 */
function etranzact_webpayment_javascript($config) {
  if ($config['etranzact_ssl']) $scheme = 'https';
  else $scheme = 'http';
  
  // Add javascript for etranzact gateway and link to webpayment framework
  drupal_add_js(drupal_get_path('module', 'etranzact') . '/etranzact.js');
  $js = "eTranzact.configure(['{$config['terminal_id']}', '$description']);";
  drupal_add_js($js, 'inline');
//  dpm($_SERVER);
  drupal_add_js(array('etranzact' => array('baseurl' => $scheme . '://' . $_SERVER['HTTP_HOST'] . url('etranzact/card'))), 'setting');
}


/**
 * Implementation of hook_status()
 * 
 * Return the status of a particular transaction specified by transactionId
 * 
 * @param $trxId  Internally generated transaction id
 * @param $config Configuration options for this gateway
 * @return A keyed array with the following elements
 * <code>
 *    array(
 *      'status'    => 'success' | 'fail' | 'pending' | 'notrans' | 'error',
 *      'amount'    => '<the_actual_amount>',
 *      'error'     => '<description_of_error_if_failed>',
 *      'errorcode' => '<code_for_the_error>',
 *    );
 * </code>
 */
function etranzact_status($trnxId, $config) {
  return etranzact_test_status();
  require_once('nusoap/lib/nusoap.php');
  $wsdlfile = "http://webpay.etranzactng.com/webpayservice_pilot/webpay.asmx";
  $cadpId = $config['card_partner_id'];
  $mertId = $config['merchant_id'];
  $msg = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <getStatus xmlns="http://webpay.etranzactng.com/webpay/">
          <CADPID>'.$cadpId.'</CADPID>
          <MERTID>'.$mertId.'</MERTID>
          <TXNREF>'.$trnxId.'</TXNREF>
        </getStatus>
      </soap:Body>
    </soap:Envelope>';

  $soap = new soapclientw($wsdlfile);
  if (!empty($config['proxyhost'])) {
    $soap->setHTTPProxy($config['proxyhost'],$config['proxyport'],$config['proxyuser'],$config['proxypass']);
  }
  $result = $soap->send($msg,'http://webpay.etranzactng.com/webpay/getStatus',60);
  
  $status = array();
  if ($result) {
    $statusresult = _etranzact_response_parse($result['getStatusResult']);
    
    $status['errorcode'] = $statusresult['rspcode'];
    $status['error'] = ($statusresult['rspcode'][0] == 'X') ? INTERSWITCH_X_MESSAGE : $statusresult['rspdesc'];
    $status['internalerror'] = $statusresult['rspdesc'];
    $status['amount'] = $statusresult['appamt'] / 100;
    switch ($status['errorcode']) {
      case '00':  // Successful
        $status['status'] = 'success';
        break;
        
      case '09': case '10': case '11':  // Pending  ("Request in progress.", "Approved, partial.", "Approved, VIP.")
        $status['status'] = 'pending';
        break;
        
      case 'W56':  // No transaction [No Transaction Record]
        $status['status'] = 'notrans';
        break;
        
      default:  // Others - failed
        $status['status'] = 'fail';
    }
  }
  else {
    $status['status'] = 'error';
    $status['error'] = $soap->getError();
    $status['internalerror'] = $soap->getError();
    $status['errorcode'] = -1234567;
  }
  
  return $status;
}

function etranzact_test_status() {
  return (object) array(
    'errorcode' => 'T0' . rand(0, 9),
    'error'     => 'Error Message',
    'internalerror' => 'Internal Error Message',
    'amount'    => '100',
    'status'    => 'fail',
  );
}



/**
 * Admin configuration form   
 */                           
function etranzact_configure_form($config) {
  $form['etranzact_config'] = array (
    '#type' => 'fieldset',    
    '#title' => t('eTranzact'),
    '#collapsible' => FALSE,
    '#weight' => 0,
  );

  $form['etranzact_config']['terminal_id'] = array (
    '#type' => 'textfield',
    '#title' => t('Terminal ID'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => 0,
    '#size' => 40,
    '#default_value' => $config['terminal_id'],
  );

  $form['etranzact_config']['etranzact_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use SSL Encyption'),
    '#description' => t('Drupal\'s built-in HTTP client only supports SSL on PHP 4.3 compiled with OpenSSL.'),
    '#default_value' => $configuration['etranzact_ssl'],
  );
//  $form['soap_proxy']['proxyhost'] = array (
//    '#type' => 'textfield',
//    '#title' => t('Host'),
//    '#maxlength' => 255,
//    '#required' => FALSE,
//    '#size' => 40,
//  );
// 
//  $form['soap_proxy']['proxyport'] = array (
//    '#type' => 'textfield',
//    '#title' => t('Port No'),
//    '#maxlength' => 255,
//    '#required' => FALSE,
//    '#size' => 40,
//  );
// 
//  $form['soap_proxy']['proxyuser'] = array (
//    '#type' => 'textfield',
//    '#title' => t('Username'),
//    '#maxlength' => 255,
//    '#required' => FALSE,
//    '#size' => 40,
//  );
// 
//  $form['soap_proxy']['proxypass'] = array (
//    '#type' => 'password',
//    '#title' => t('Password'),
//    '#maxlength' => 255,
//    '#required' => FALSE,
//    '#size' => 40,
//  );
 
  return $form;      
}

function etranzact_configure_form_validate(&$form, &$form_state) {
  if (!is_numeric($form_state['values']['terminal_id']) || strlen($form_state['values']['terminal_id']) != 10) {
    form_set_error('terminal_id', t('Terminal ID should be a 10-digit number.'));
  }
}

/**
 * Implementation of hook_menu()
 * 
 * Used to redirect to the page to submit the form
 */
function etranzact_menu() {
  $items = array();

  $items['etranzact/card'] = array(
    'title' => 'eTranzact Webconnect',
    'description' => 'Portal to pay with eTranzact card.',
    'page callback' => 'etranzact_portal',
    'page arguments' => array('card'),
    'access arguments' => array(NULL),
    'type' => MENU_CALLBACK,
  );

  $items['etranzact/pinverify'] = array(
    'title' => 'eTranzact Webconnect',
    'description' => 'Portal to pay with eTranzact Pin.',
    'page callback' => 'etranzact_portal',
    'page arguments' => array('pin'),
    'access arguments' => array(NULL),
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
 * Implementation of hook_preprocess
 */
//function etranzact_preprocess_page(&$variables) {
//  $variables['template_files'] = array('page-etranzact');
//  dpm($variables);
//}

  
function etranzact_portal($method) {
  switch($method) {
    case 'card':
      $success = $_POST["SUCCESS"];
      if ($success == null) { //or success = "" for php
        $output .=  "<form method='POST' action='http://demo.etranzact.com/WebConnect' id='etranzact-form'>\n";
        $output .=  "<input type='hidden' name='TERMINAL_ID' value='{$_GET['TERMINAL_ID']}'>\n";
        $output .=  "<input type='hidden' name = 'TRANSACTION_ID' value='{$_GET['TRANSACTION_ID']}'>\n";
        $output .=  "<input type='hidden' name = 'AMOUNT' value='{$_GET['AMOUNT']}'>\n";
        $output .=  "<input type='hidden' name = 'DESCRIPTION' value='{$_GET['DESCRIPTION']}'>\n";
        $output .=  "<input type='hidden' name = 'RESPONSE_URL' value='" . url('etranzact/response') . "'>\n";
        $output .=  "<input type='hidden' name = 'LOGO_URL' value='https://demo.etranzact.com/eTranzact/Interfaces/images/mfmlogo.gif'>\n";
        $output .=  "</form>\n";
        $output .=  "<script language='javascript'>\n";
        $output .=  '$(document).ready(function() {';
        $output .=  '  $("form#etranzact-form").submit();';
        $output .=  "});\n";
        $output .=  "</script>\n";
      }
      else if ($success == "0") {
        //deal with successful transaction
        $output .=  "Transaction Successfull";
        session_register("transId");
      }
      else { //Deal with Timeout Here, Transaction ID no more valid
        $output .=  "Error while requesting for transaction authorisation, Transaction ID no more valid ";
      }
      break;
      
    case 'pin':
      break;
      
    default:
      $output = "Unknown payment method";
  }
  return $output;  
}

//function _etranzact_response_error($error_code) {
//  $errors = include_once('etranzact_errorcodes.inc');
//  return $errors[$error_code];
//}

//function _etranzact_response_amount($webpay_response) {
  //$output .=  $webpay_response."\n";
//  $webpay_response = str_replace("&amp;", "&", $webpay_response);
//  $arr_response = split("&", $webpay_response);

//  if (count($arr_response) == 8) {
//    $arr_code = split("=", $arr_response[5]);
//    return $arr_code[1];
//  }
//  else if (count($arr_response) == 2) {
//    $arr_code = split("=", $arr_response[0]);
//    return $arr_code[6];
//  }
//  else {
//    return count($arr_response);
//  }
//}

function _etranzact_response_parse($response) {
  /* 'dtime=2/17/2010 10:19:38 PM&
   *  pan=007 &
   *  froacc=&
   *  toacc=&
   *  paytxncode=&
   *  appamt=0&
   *  rspcode=X01&
   *  rspdesc=Your Card has been Hotlisted. Please contact your bank'
   * 
   * rspcode=W56&rspdesc=[No Transaction Record]
   */
  $res = explode('&', $response);
  $rest = array();
  if (count($res) == 8) {
    foreach ($res as $value) {
      list($k, $v) = explode('=', $value);
      $rest[trim($k)] = trim($v);
    }
    return $rest;
  }
  else if (count($res) == 2) {
    foreach ($res as $value) {
      list($k, $v) = explode('=', $value);
      $rest[trim($k)] = trim($v);
    }
    return $rest;
//    $vals = explode('=', $res[0]);
//    $keys = explode('=', $res[1]);
//    return array_combine($keys, $vals);
  }
}

